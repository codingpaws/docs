---
title: Slack off more at work
description: Here’s why you should exit Slack regularly at work
date: 2022-04-30
home: true
---

If you’ve worked at a large or remote company, you’ve most likely used an
instant messaging app like Skype, Teams, or Slack. These apps allow async
communication like emails. But they also allow for real-time discussions
like an IRC messenger.

When you send an email, you wouldn’t expect an immediate answer from the
recipient. Emails sometimes take a few seconds or minutes to arrive. Some
email inboxes scan all incoming emails or have to process attachments.
These delays are inherent to emails and many emails don’t require instant
attention.

In contrast, Slack messages (as an example), often arrive in less than a
second because they never leave Slack’s servers. Each mention or direct
message makes the too-familiar knock-knock brush resound from your
speakers.

To my surprise, some workplaces strongly suggest or even require that you
always be online when you’re working. Even if you’re workplace is less
strict in this regard, you’ll presumably have Slack running out of habit
for most of your day. At least I do, or rather, I did.

While I know about focus, interruptions, and deep work, I would rarely
turn off notifications. A colleague or my team’s product manager might
happen to message me with something urgent. Except for some jobs or
situations, there is no harm in not replying for an hour while you’re
enjoying lunch or sitting in a meeting. So why would you want your
attention to be always available at one `@channel` or direct message?

The potential cost of being interrupted by a Slack message (or email) to
your productivity is very high. Therefore, I try to reserve a daily 30 to
90-minute time slot, set my status to something like “🌌 Deep work; I
might not reply now!”, and disable notifications. You can even close
Slack if you have a checking habit.

Being able to work without interruptions is a great feeling. And after
the focus time is up, you can reopen Slack and answer all texts if you’ve
even received any. That’s why you should _Slack off_ more at work.
