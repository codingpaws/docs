---
title: Faster updates with fancy spinners
description: GDK go round and round and round...
date: 2024-08-31
rss: true
home: true
---

Practically everyone, who contributes code to (or reviews changes of) the GitLab Rails monolith, uses a single piece of developer tooling to bootstrap and manage their development environment: the GitLab Development Kit, or GDK for short. It bundles eleven core services that are required to boot a local GitLab instance, and many many more optional services.

GDK is an essential—if not **the** essential—piece of local developer tooling within GitLab. That’s why it’s imperative that it is stable and fast. Over the years, GDK has become quite stable except for some usually foreseeable situations. However, it can be quite slow, particularly on older laptops, even if they meet its minimum system requirements.

One of the slowest and most frequently executed GDK commands is the `gdk update` command. It performs a self-update followed by updates of all enabled services.

We [recommend](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/4cb6a5caea58683f44c6669f8d84284649e637e1/doc/gdk_commands.md?plain=0#update-gdk) that active contributors, especially GitLab team members, who work on the Rails monolith, run `gdk update` daily or every few days at minimum. Some contributors may even run `gdk update` more frequently to [iterate](https://handbook.gitlab.com/handbook/values/#iteration) faster on the latest changes.

On average, an update takes around **19 minutes**. Most GitLab engineers use recent, powerful M-series MacBooks, so the average update duration for them will be lower. Though, that means that many contributors from the wider community will have a significantly worse experience developing with GDK. In the spirit that [everyone can contribute](https://handbook.gitlab.com/handbook/company/mission/), <mark>it’s hard to contribute if every contribution begins with over ten minutes of waiting</mark>.

And while most engineers will spend the wait time on other tasks or taking a break to refill their drink of choice (iced tea for me!), it’s still a notable inefficiency. And [we dislike inefficiencies](https://handbook.gitlab.com/handbook/values/#efficiency)!

Therefore, we’ve been working on improving both the `gdk update` experience and performance. Something that started as a silly DX proposal by me in Slack three months ago has finally been enabled three weeks ago: [spinners](https://gitlab.com/gitlab-org/gitlab-development-kit/-/merge_requests/3814)!

| Before                                                                                       | After                                                                                        |
| -------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------- |
| <video src="/vid/2024-08-30_gdk-old.webm" controls muted height="1080" width="1700"></video> | <video src="/vid/2024-08-30_gdk-new.webm" controls muted height="1080" width="1700"></video> |

<style>td { width: 50%; } @media (min-width: 1100px) { table { margin: 0 -100px; } }</style>

<small class="caption">Note: the video on the right does more steps for illustrative purposes (and more spinners, heh!), the left video would just keep outputting text into your terminal.</small>

Aside from spinners just looking absolutely ✨ fancy ✨ (thanks to Piotr Murach’s [tty-spinner](https://github.com/piotrmurach/tty-spinner) gem), which the frontend engineer in me loves, we could finally start [parallelizing update steps](https://gitlab.com/gitlab-org/gitlab-development-kit/-/merge_requests/3726) a week ago. Now that we are hiding the linear output from developers, GDK can, for example, download dependencies for one project while compiling another. In the last few seconds of the after video above, you can even see this already.

All in all, these changes sped up the update duration by around 30%!

We’re not done yet though. Next up, we’re working on providing [pre-compiled binaries](https://gitlab.com/groups/gitlab-org/-/epics/9729) for GDK services like Gitaly (GitLab’s Git RPC service) or Workhorse (a smart reverse proxy). This will not only massively speed up updates once again, but also make GDK much more stable for most developers because their local environments won’t have to support compiling GDK service binaries.
