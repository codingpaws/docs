---
title: Migrating SiegeGG to Vue
description: From Laravel to Nuxt, for better performance and features
date: 2022-09-20
rss: true
home: true
---

At the beginning of 2022, a few months after SiegeGG was
[acquired by Gfinity][acquisition], we sat in a meeting discussing the
strategy of [our website][siegegg]. One of the key points we talked about
was how we could best integrate Gfinity tech into the website. Their
experience with editorial content, especially on a technical level, was
far exceeding ours.

Most of Gfinity’s existing sites were already developed in Vue or in the
process of being migrated to Vue. SiegeGG was already using Vue for, for
example, the search box at the top of the website. And creating reactive
UIs, like that search, without a framework is expensive and skyrockets
maintenance costs. Therefore, we decided to completely migrate SiegeGG to
Vue in the coming months.

![](/img/migrating-siegegg-to-vue/navbar-search.png)

Our plan to migrate SiegeGG to Vue was threefold:

1. Migrate [SiegeGG Pro][pro]  
   _Pro is our enterprise product aimed at professional players,
   analysts, and broadcast talent. A migration to Vue allowed us to more
   easily add new features and improve Pro’s performance._
1. Migrate the [public website][siegegg]  
   _Apart from integrating Gfinity tech, we wanted (and still want) to
   improve the performance and experience of the SiegeGG website._
1. Integrate Gfinity tech

As first step, I created [SiegeGG UI][ui], our UI component library, on
March 24th, 2022. Similar to [GitLab UI][glui], it would encompass all
base components used on SiegeGG to simplify design changes and improve
UI consistency. The component library is publicly hosted on GitLab and
uses their NPM package registry, to which we have released
[48 versions][uiver] at time of writing.

During the [Charlotte Major][charlotte], I started rewriting the entire
codebase of SiegeGG Pro in Vue. More specifically, in [Nuxt 3][nuxt3].
Although Nuxt was still in early development in May—which would cause me
some major headace—, it very much simplified the process of writing
features for Pro.

After Pro was mostly migrated, in the evening of June 2nd, 2022, I
committed the initial commit in the SiegeGG New repository. This marked
the kick-off of the second step in the migration plan. After an
internally busy time, a personally tough time, and two weeks of vacation,
I was ready to push the website migration forward.

However, with other development work blocking parts of my capacity before
and during the [Berlin Major][berlin] (and our dog being sick for a few
weeks), I couldn’t focus all of my energy on the migration,
unfortunately. So, on the Monday after the Berlin Major, I started using
a new short-term planning system for myself, which has helped me to focus
on the important next steps of the migration and regulate my output.

I made good progress and could already estimate a possible release date.
Three weeks after the major’s playoffs, I had checked off enough tasks on
the migration that I [teased][teaser] the release of SiegeGG New. While
I didn’t meet all my goals to release it to all users, I enabled SiegeGG
New on Wednesday and Thursday for German users that week. Back then,
everyone in any region could’ve tried it out at [new.siege.gg][new].

As of September 13th, the public SiegeGG website is fully powered by the
Nuxt 3 app. And, while the old site is still functional, we’ll soon start
removing functionality from it to reduce our maintenance scope.

## What we’ve learned

While the overall migration is still in full swing, it’s time for a
preliminary conclusion.

I started my work on SiegeGG UI in late March. When I’m posting this,
that’s exactly 6 months (à 30 days) ago. Had I only focused on migrating
all SiegeGG web products to Vue, the migration would’ve complete by now.
However, the old lesson “life always gets in your way” holds true, as
always. Be it personal life, or other work responsibilities; they’ll
always distract you and thwart your plans.

Another thing, I unfortunately didn’t account for enough, was the effect
of the migration on my peers and our users. I excitedly communicated our
migration efforts to the team (and [publicly][teaser]) but I didn’t talk
enough about how their workflow could or would be affected. Specifically
our CMS (content management system) would stay behind on the old
Laravel-based website for the time being. This led to some fair stress
for multiple team members as everyone on the team relied on features only
available in the CMS.

After we’ve spent multiple months on the Laravel to Nuxt migration, I
felt excitement (and pressure) about finally delivering.

Delivering is something you can’t reasonably do while you’re changing the
programming language or architecture of your software. For a few months,
we couldn’t add new features or improve existing ones on the SiegeGG
website. If we did, it would’ve doubled our work. That’s why I initially
hesitated to agree to a complete migration.

After SiegeGG New has been live for about a week, many new and related
problems have surfaced. For example, logging in to our CDN no longer
worked because the OAuth2 API is still part of the Laravel app. What’s
more, I accidentally didn’t migrate a few features. Other features aren’t
part of SiegeGG New because it fundamentally works differently, such as
the stats “Maps” and “Disables” on our competitions. I’m out of capacity
for at least the next week but new issues pop up daily.

## Closing thoughts

Over the course of about six months, we’ve migrated two SiegeGG products
from Laravel/Blade to Nuxt/Vue with only one person actively working on
the code. More than that, SiegeGG Pro is in a state where we can offer
[free 1 month trials][pro] to professional Rainbow Six esports teams.

I’m looking optimistic towards a future where we’ll finally be able to
work on the features and design improvements of the SiegeGG website
again. This time on faster, better scaling software. Even if the way
there was pretty long and a bit draining.

<style>
    article img {
        display: flex;
        margin: auto;
    }
</style>

[acquisition]: https://www.gfinityplc.com/gfinity-acquires-siegegg-rainbow-six-siege-website-social-channel-acquisition-digital-media-gdm-investor-news-esports-uk/
[siegegg]: https://siege.gg
[ui]: https://gitlab.com/SiegeGG/ui
[glui]: https://gitlab.com/gitlab-org/gitlab-ui
[pro]: https://pro.siege.gg
[charlotte]: https://siege.gg/competitions/404
[nuxt3]: https://v3.nuxtjs.org/
[uiver]: https://gitlab.com/SiegeGG/ui/-/packages
[teaser]: https://twitter.com/KevSlashNull/status/1564673147542163456
[new]: https://new.siege.gg/
[berlin]: https://new.siege.gg/competitions/418
