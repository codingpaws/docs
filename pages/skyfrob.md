---
title: skyfrob(3)
description: frobnicate (obfuscate) a bluesky skeet
date: 2024-11-28
---

<link rel="stylesheet" href="/css/man.css" />

<div class="man">

`skyfrob(3)` — ATProto Functions Manual

## NAME

skyfrob - frobnicate (obfuscate) a bluesky skeet

## SYNOPSIS

```c
#include <atproto.h>

skeet_t *skyfrob(skeet_t s);
```

## DESCRIPTION

The `skyfrob()` function obfuscates the content of a given skeet `s` by
exclusively-ORing each character with the number `69`. The effect can be
reversed by using `skyfrob()` on the obfuscated skeet data.

Note that this function is not a proper encryption routine as the XOR
constant is fixed, and is suitable only for hiding mildly spicy skeets.
<span class="js-paw" />

## RETURN VALUE

The `skyfrob()` function returns a pointer to the obfuscated skeet.

## ATTRIBUTES

For an explanation of the terms used in this section, see
`attributes(7)`.

<pre>
┌─────────────────────┬───────────────┬─────────┐
│ Interface           │ Attribute     │ Value   │
├─────────────────────┼───────────────┼─────────┤
│ skyfrob()           │ Thread safety │ MT-Safe │
└─────────────────────┴───────────────┴─────────┘
</pre>

## STANDARDS

ATProto.

## SEE ALSO

`bstring(3)`, [`skyfry(3)`](./skyfry.md)

<div style="display: flex; justify-content: space-between;">
<div>ATProto man-pages 6.7</div>
<div>2024-11-28</div>
<code>skyfrob(3)</code>
</div>

</div>
