---
title: skyfry(3)
description: randomize a skeet's content
date: 2024-11-28
---

<link rel="stylesheet" href="/css/man.css" />

<div class="man">

`skyfry(3)` — ATProto Functions Manual

## NAME

skyfry - randomize a skeet's content

## SYNOPSIS

```c
#include <atproto.h>

char **skyfry(skeet_t s);
```

## DESCRIPTION

The `skyfry()` function randomizes the contents of the skeet by randomly
swapping characters in all its strings, including any provided embed. The
result is an anagram of `skeet`.

Excessive use of `skyfry()` may cause the user's skyline to be fried
entirely. <span class="js-paw" />

## RETURN VALUE

The `skyfry()` function returns a pointer to all randomized strings in
order of appearance.

## ATTRIBUTES

For an explanation of the terms used in this section, see
`attributes(7)`.

<pre>
┌─────────────────────┬───────────────┬─────────┐
│ Interface           │ Attribute     │ Value   │
├─────────────────────┼───────────────┼─────────┤
│ skyfry()            │ Thread safety │ MT-Safe │
└─────────────────────┴───────────────┴─────────┘
</pre>

## STANDARDS

ATProto.

## SEE ALSO

[`skyfrob(3)`](./skyfrob.md), `string(3)`

<div style="display: flex; justify-content: space-between;">
<div>ATProto man-pages 6.7</div>
<div>2024-11-28</div>
<code>skyfrob(3)</code>
</div>

</div>
