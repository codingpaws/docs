package app

import (
	"fmt"
	"os"
	"strings"
	"time"

	"codingpa.ws/internal/feedgen"
	"codingpa.ws/internal/md"
)

const sitemapPath = "public/sitemap.xml"
const rssPath = "public/feed.xml"

var feedList = feedgen.NewList()

func generateSitemap() error {
	start := time.Now()
	err := os.WriteFile(sitemapPath, feedList.ToSitemap(), 0766)

	if err != nil {
		return err
	}

	fmt.Printf("%s generated \x1b[34;1m%s\x1b[0m \x1b[2m[%s]\x1b[0m\n", checkSymbol, rssPath, time.Since(start))

	return nil
}

func generateRssFeed() error {
	start := time.Now()

	rss, err := feedList.ToRss(strings.Replace(rssPath, "public", "", 1))

	if err != nil {
		return err
	}

	err = os.WriteFile(rssPath, rss, 0766)

	if err != nil {
		return err
	}

	fmt.Printf("%s generated \x1b[34;1m%s\x1b[0m \x1b[2m[%s]\x1b[0m\n", checkSymbol, sitemapPath, time.Since(start))

	return nil
}

func addRssEntry(meta md.Meta, newPath string) {
	date := meta.Date

	if date == nil {
		return
	}

	url := fmt.Sprintf("https://codingpa.ws/%s", strings.Replace(newPath, "public/", "", 1))
	feedList.Add(
		meta.Title,
		url,
		meta,
		*date,
	)
}
