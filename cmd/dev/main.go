package main

import (
	"context"
	"fmt"
	"io/fs"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	"codingpa.ws/internal/serve"
	"github.com/fsnotify/fsnotify"
	"golang.org/x/sync/errgroup"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	errgroup := new(errgroup.Group)

	errgroup.Go(func() error {
		return serve.Start("public")
	})

	errgroup.Go(func() error {
		defer cancel()

		for {
			out, err := exec.CommandContext(ctx, "templ", "generate").CombinedOutput()
			if err != nil {
				fmt.Println(string(out))
				return err
			}
			cmd := exec.CommandContext(ctx, "sh", "build.sh")
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			err = cmd.Run()
			if err != nil {
				return err
			}
			err = os.WriteFile("public/.hot-reload", []byte(time.Now().Format(time.RFC3339)), 0664)
			if err != nil {
				return err
			}

			err = startListener(ctx)
			if err != nil {
				return err
			}
			time.Sleep(time.Millisecond * 200)
		}
	})

	err := errgroup.Wait()
	if err != nil {
		log.Fatalln(err)
	}
}

func startListener(ctx context.Context) error {
	var closed bool
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return err
	}
	defer func() {
		if !closed {
			closed = true
			watcher.Close()
		}
	}()

	go func() {
		<-ctx.Done()
		if closed {
			return
		}
		closed = true
		watcher.Close()
	}()

	paths := []string{"app", "cmd", "pages", "static", "templates"}

	for _, path := range paths {
		err = filepath.Walk(path, func(path string, info fs.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if info.IsDir() {
				return watcher.Add(path)
			}
			return nil
		})
		if err != nil {
			return err
		}
	}

chfor:
	for {
		select {
		case event, ok := <-watcher.Events:
			if !ok {
				break chfor
			}
			if event.Has(fsnotify.Write) {
				fmt.Println("changed", event.Name)
				return nil
			}
		case err, ok := <-watcher.Errors:
			if !ok {
				break chfor
			}
			return fmt.Errorf("fsnotify: %w", err)
		}
	}

	return nil
}
