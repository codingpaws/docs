package templates

import (
	"codingpa.ws/internal/links"
	"codingpa.ws/internal/md"
	"github.com/a-h/templ"
)

type LinkedMeta struct {
	md.Meta
	Link string
}

func Render(meta Meta, homeLinks []LinkedMeta) (_ templ.Component, err error) {
	var globalLinks []links.GlobalLink
	if globalLinks, err = links.Get(); err != nil {
		return
	}
	return page(meta, globalLinks, homeLinks), nil
}
