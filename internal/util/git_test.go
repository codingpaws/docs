package util_test

import (
	"os"
	"testing"

	"codingpa.ws/internal/util"
	"github.com/stretchr/testify/require"
)

func TestGitRevisionForCache(t *testing.T) {
	expectedSHA, ok := os.LookupEnv("CI_COMMIT_SHA")
	if !ok {
		t.Skip("CI_COMMIT_SHA not present in env")
	}

	rev, err := util.GitRevisionForCache()
	require.NoError(t, err)
	require.Equal(t, expectedSHA[8:16], rev)
}
